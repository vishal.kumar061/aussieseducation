﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RealEstate.Config
{
    public class StripeAPI
    {

        public void Authentication()
        {
            CreateCharge();
            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["ApiKey"];
            var options = new RequestOptions
            {
                ApiKey = "sk_test_4eC39HqLyjWDarjtT1zdp7dc"
            };
            var service = new ChargeService();
            Charge charge = service.Get(
              "ch_1JGxmU2eZvKYlo2CO4fDJ5CP", null,
              options
            );
        }

        public void CreateCharge()
        {

            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["ApiKey"];

            // `source` is obtained with Stripe.js; see https://stripe.com/docs/payments/accept-a-payment-charges#web-create-token

            var CustomerCreateOptions = new CustomerCreateOptions
            {
                Name = "Jenny Rosen",
                Address = new AddressOptions
                {
                    Line1 = "510 Townsend St",
                    PostalCode = "98140",
                    City = "San Francisco",
                    State = "CA",
                    Country = "US",
                },
            };
            var CustomerService = new CustomerService();
            var customer = CustomerService.Create(CustomerCreateOptions);
            var resp = customer.StripeResponse;

            var ChargeCreateOptions = new ChargeCreateOptions
            {
                Amount = 2000,
                Currency = "usd",
                Source = "tok_mastercard",
                Description = "My First Test Charge (created for API docs)",
                
            };
            var ChargeService = new ChargeService();
            ChargeService.Create(ChargeCreateOptions);
        }


    }
}