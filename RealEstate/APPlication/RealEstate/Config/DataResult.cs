﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Config
{
    /// <summary>
    /// Respresents a data result object
    /// </summary>
    public class DataResult
    {
        /// <summary>
        /// Gets or set the list of errors
        /// </summary>
        public IList<string> Errors { get; set; }

        #region Ctor

        public DataResult()
        {
            this.Errors = new List<string>();
        }

        #endregion

        /// <summary>
        /// Gets or sets whether the action result was success or failure
        /// </summary>
        public bool Success
        {
            get { return (this.Errors.Count == 0); }
        }

        /// <summary>
        /// Add error message
        /// </summary>
        /// <param name="error"></param>
        public void AddError(string error)
        {
            this.Errors.Add(error);
        }

        /// <summary>
        /// Gets a strings of all the errors delimited by | 
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public string ErrorMessages
        {
            get
            {
                return this.Errors.ToDelimitedString("/");
            }
        }
        public APIStatusCode StatusCode { get; set; }


        //----------------Cashfree model------------------
        public string message { get; set; }
        public string status { get; set; }
        public string subCode { get; set; }
    }

    public class DataResult<T> : DataResult
    {
        /// <summary>
        /// Gets or set the data item
        /// </summary>
        public T Data { get; set; }

    }

    public enum APIStatusCode
    {
        OK = 200,
        EntityDoesNotExists = 403,
        DuplicateEntity = 442,
        InvalidEmail = 443,
        ErrorSendingMail = 445,
        Oops = 446,
        Unauthorized = 401,
        InvalidOtp = 409
    }

    public class APIStatusMessage
    {
        public const string EntityDoesNotExists = "Entity Does Not Exists";
        public const string DuplicateEntity = "Duplicate Entity";
        public const string InvalidEmail = "Invalid Email-Id";
        public const string ErrorSendingMail = "Error sending mail";
        public const string Oops = "Oops Something went wrong !!";
        public const string Unauthorized = "Unauthorized";
        public const string InvalidOtp = "Invalid OTP";
    }
}
