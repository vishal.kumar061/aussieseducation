﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstate.Models.Project
{
    public class advancesearch
    {
        public IList<string> intake { get; set; }
        public IList<string> year { get; set; }
        public IList<string> programlevel { get; set; }
        public IList<string> country { get; set; }
        public IList<string> studyarea { get; set; }
        public IList<string> disciplinearea { get; set; }
        public IList<string> elp { get; set; }
        public IList<string> requirements { get; set; }
        
    }
}