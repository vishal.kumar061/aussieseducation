﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing;
using System.Web.Routing;

namespace RealEstate
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "account", action = "Index", id = UrlParameter.Optional }
            );
            //------------------------------Web Home-----------------------
            routes.MapRoute(
             name: "Home",
             url: "web/index/",
             defaults: new { controller = "Web", action = "Index" }
         );
            //------------------------------Account-----------------------

            routes.MapRoute(
               name: "logout",
               url: "account/logout/",
               defaults: new { controller = "Account", action = "Logout" }
           );

            routes.MapRoute(
             name: "login",
             url: "account/index/",
             defaults: new { controller = "Account", action = "Index" }
         );


            //------------------------------User-----------------------
            routes.MapRoute(
              name: "UserList",
              url: "User/Index/",
              defaults: new { controller = "User", action = "Index" }
          );
            routes.MapRoute(
              name: "AddEditUser",
              url: "User/AddEditUser/{id}",
              defaults: new { controller = "User", action = "AddEditUser", id = UrlParameter.Optional }
          );


            //------------------------------university-----------------------
            routes.MapRoute(
            name: "university",
            url: "university/SearchProgram/",
            defaults: new { controller = "university", action = "SearchProgram" }
            );

        }
    }
}
