namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CFAALL2021_1
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(4000)]
        public string key { get; set; }

        public string value { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte type { get; set; }

        public long? rownum { get; set; }
    }
}
