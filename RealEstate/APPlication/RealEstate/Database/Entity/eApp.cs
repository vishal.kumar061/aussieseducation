using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace RealEstate.Database.Entity
{
    public partial class eApp : DbContext
    {
        public eApp()
            : base("name=eApp")
        {
        }

        public virtual DbSet<MA_ActionLog> MA_ActionLog { get; set; }
        public virtual DbSet<MA_Category> MA_Category { get; set; }
        public virtual DbSet<MA_Filter> MA_Filter { get; set; }
        public virtual DbSet<MA_Role> MA_Role { get; set; }
        public virtual DbSet<MA_SubCategory> MA_SubCategory { get; set; }
        public virtual DbSet<MA_University> MA_University { get; set; }
        public virtual DbSet<MA_User> MA_User { get; set; }
        public virtual DbSet<RoleUrlAccess> RoleUrlAccesses { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserToken> UserTokens { get; set; }
        public virtual DbSet<CFAALL2021> CFAALL2021 { get; set; }
        public virtual DbSet<CFAALL2021_1> CFAALL2021_1 { get; set; }
        public virtual DbSet<CFALL2022> CFALL2022 { get; set; }
        public virtual DbSet<CFALL2022_1> CFALL2022_1 { get; set; }
        public virtual DbSet<column> columns { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MA_Role>()
                .HasMany(e => e.RoleUrlAccesses)
                .WithRequired(e => e.MA_Role)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MA_Role>()
                .HasMany(e => e.UserRoles)
                .WithOptional(e => e.MA_Role)
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserRoles)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<MA_User>()
                .HasMany(e => e.UserTokens)
                .WithOptional(e => e.MA_User)
                .HasForeignKey(e => e.UserId);
        }
    }
}
