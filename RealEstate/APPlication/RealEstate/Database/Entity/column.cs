namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class column
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string column1 { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(200)]
        public string column2 { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string column3 { get; set; }
    }
}
