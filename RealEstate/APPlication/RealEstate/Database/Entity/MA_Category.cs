namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_Category
    {
        public int Id { get; set; }

        public int? CategoryId { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
