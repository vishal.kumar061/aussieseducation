namespace RealEstate.Database.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MA_University
    {
        public int Id { get; set; }

        public string ids { get; set; }

        public string Name { get; set; }

        public string Concentration { get; set; }

        public string StudyLevelId { get; set; }

        public string Studylvl { get; set; }

        public string Country { get; set; }

        public string University { get; set; }

        public string Country1 { get; set; }

        public string Duration { get; set; }

        public string UniversityId { get; set; }

        public string CountryId { get; set; }

        public string EligibleUniversityStateId { get; set; }

        public string SatRequired { get; set; }

        public string GmatRequired { get; set; }

        public string ActRequired { get; set; }

        public string GreRequired { get; set; }

        public string IeltsRequired { get; set; }

        public string ToeflRequired { get; set; }

        public string PteRequired { get; set; }

        public string DETRequired { get; set; }

        public string IeltsOverall { get; set; }

        public string ToeflScore { get; set; }

        public string PteScore { get; set; }

        public string SatScore { get; set; }

        public string ActScore { get; set; }

        public string GreScore { get; set; }

        public string GmatScore { get; set; }

        public string DETScore { get; set; }

        public string TutionFee { get; set; }

        public string backlog { get; set; }

        public string Currency { get; set; }

        public string Amount { get; set; }

        public string IELTSWaiverScoreCBSE { get; set; }

        public string IELTSWaiverScoreState { get; set; }

        public string IeltsNoBandLessThan { get; set; }

        public string TOEFLNoSectionLessThan { get; set; }

        public string PTENoSectionLessThan { get; set; }

        public string FifteenYearsEducation { get; set; }

        public string ScholarshipAvailable { get; set; }

        public string Intakes { get; set; }

        public string UniversityOrder { get; set; }

        public string EntryRequirementTwelfthOutOf4 { get; set; }

        public string EntryRequirementTwelfthOutOf5 { get; set; }

        public string EntryRequirementTwelfthOutOf7 { get; set; }

        public string EntryRequirementTwelfthOutOf10 { get; set; }

        public string EntryRequirementTwelfthOutOf45 { get; set; }

        public string EntryRequirementTwelfthOutOf100 { get; set; }

        public string EntryRequirementUgOutOf4 { get; set; }

        public string EntryRequirementUgOutOf5 { get; set; }

        public string EntryRequirementUgOutOf7 { get; set; }

        public string EntryRequirementUgOutOf10 { get; set; }

        public string EntryRequirementUgOutOf100 { get; set; }

        public string WebomatricsNationalRanking { get; set; }

        public string WebomatricsWorldRanking { get; set; }

        public string Ranking { get; set; }

        public string RankingFrom { get; set; }

        public string IsIndirect { get; set; }

        public string commissionMode { get; set; }

        public string commissionAmount { get; set; }

        public string CommissionCurrency { get; set; }

        public string IntakesClosed { get; set; }

        public string IsExtraChoicesOfPrograms { get; set; }

        public string Mediator { get; set; }

        public string IsOnlineCourse { get; set; }

        public string IntakeDeadLines { get; set; }

        public string WorkExp { get; set; }

        public string CategoryId { get; set; }

        public string SubCategoryId { get; set; }

        public string EligibleUniversityCountryId { get; set; }



        public string logo { get; set; }
        public string Web_Url { get; set; }
        public string Campus { get; set; }
        public string Course_Url { get; set; }
        public string coursefinder_url { get; set; }
        public string Entry_Requirements { get; set; }
        public string Application_Fees { get; set; }
    }
}
