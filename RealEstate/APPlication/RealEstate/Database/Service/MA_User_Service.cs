﻿using LinqToExcel;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace RealEstate.Database.Service
{
    public interface IMA_User_Service
    {
        DataResult<Responce_Auth_Model> AccessToken(Req_Auth_Model model);
        IList<MA_User> Get();
        IList<MA_User> GetAll();
        MA_User Get(int id);
        MA_User GetByPhone(string Phone);
        IList<MA_Role> GetRole();
        DataResult<UserModel> AddEditUser(UserModel model);
        DataResult<UserModel> GetUser(int userid);
        DataResult<string> DeleteUser(int id);
        DataResult<string> SignUp(SignUpModel model);
        DataResult<string> ForgetPassword(ForgetPasswordModel model);
    }
    public class MA_User_Service : IMA_User_Service
    {
        private readonly eApp Context;
        private readonly IEmailService EmailService;

        public MA_User_Service()
        {
            Context = new eApp();
            EmailService = new EmailService();
        }

        public IList<MA_User> Get()
        {
            return Context.MA_User.Where(x => x.IsActive == true).OrderBy(x => x.Name).ToList();
        }
        public IList<MA_User> GetAll()
        {
            return Context.MA_User.OrderBy(x => x.Name).ToList();
        }
        public MA_User Get(int id)
        {
            return Context.MA_User.Where(x => x.Id == id).FirstOrDefault();
        }
        public MA_User GetByPhone(string Phone)
        {
            return (from x in Context.MA_User where x.Phone == Phone && x.IsActive == true select x).FirstOrDefault();
        }
        public MA_User Get(string Email)
        {
            return (from x in Context.MA_User
                    where
                    x.Email.ToLower().Trim() == Email.ToLower().Trim()
                     && x.IsActive == true
                    select x).FirstOrDefault();
        }
        public IList<MA_User> GetAllByEmail(string Email)
        {
            return (from x in Context.MA_User
                    where
                    x.Email.ToLower().Trim() == Email.ToLower().Trim()
                     && x.IsActive == true
                    select x).ToList();
        }
        public IList<MA_Role> GetRole()
        {
            return Context.MA_Role.ToList();
        }


        public DataResult<string> SignUp(SignUpModel model)
        {
            var result = new DataResult<string>();
            try
            {
                var user = this.Get(model.Email);

                if (user == null)
                {
                    var rand = new Random();
                    var password = rand.Next().ToString().Replace(".", "").Substring(0, 5);

                    var role = new List<UserRole>();
                    role.Add(new UserRole { RoleId = 2 });
                    var entity = new MA_User
                    {
                        Name = model.Name,
                        Email = model.Email,
                        Username = model.Email,
                        IsActive = true,
                        Password = password,
                        Phone = model.Phone,
                        UserRoles = role
                    };
                    Context.MA_User.Add(entity);
                    Context.SaveChanges();
                    //send email with password

                    string Emailbody = string.Empty;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Utility/Mailer/Welcome.html")))
                    {
                        Emailbody = reader.ReadToEnd();
                    }
                    Emailbody = Emailbody.Replace("{user}", model.Name);
                    Emailbody = Emailbody.Replace("{username}", model.Email);
                    Emailbody = Emailbody.Replace("{password}", password);

                    var msg = EmailService.email(new string[] { model.Email }, Emailbody, "BRKS: WELCOME !!");

                    if (msg.Success)
                        result.Data = "An email with password has been sent to your email.";
                    else
                        result.Data = "Your account is created but we could not send you mail due to some techinical error. Please contact administrator.";

                    result.StatusCode = APIStatusCode.Oops;
                    return result;
                }
                else
                {
                    result.Errors.Add("User with same email already exist");
                    result.StatusCode = APIStatusCode.DuplicateEntity;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = APIStatusCode.Oops;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                return result;
            }
        }
        public DataResult<string> ForgetPassword(ForgetPasswordModel model)
        {
            var result = new DataResult<string>();
            try
            {
                var user = this.Get(model.Email);
                if (user != null)
                {
                    //send email with password
                    string Emailbody = string.Empty;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Utility/Mailer/Welcome.html")))
                    {
                        Emailbody = reader.ReadToEnd();
                    }
                    Emailbody = Emailbody.Replace("{user}", user.Name);
                    Emailbody = Emailbody.Replace("{username}", user.Username);
                    Emailbody = Emailbody.Replace("{password}", user.Password);

                    var msg = EmailService.email(new string[] { user.Email }, Emailbody, "BRKS: Account Password !!");

                    if (msg.Success)
                        result.Data = "An email with password has been sent to your registered email.";
                    else
                        result.Data = "Sorry we could not send you mail due to some techinical error. Please contact administrator.";

                    result.StatusCode = APIStatusCode.Oops;
                    return result;
                }
                else
                {
                    result.Errors.Add("Invalid Username");
                    result.StatusCode = APIStatusCode.DuplicateEntity;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = APIStatusCode.Oops;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                return result;
            }
        }
        public DataResult<Responce_Auth_Model> AccessToken(Req_Auth_Model model)
        {
            var result = new DataResult<Responce_Auth_Model>();

            var user = (from x in Context.MA_User
                        where
                        x.Username.ToLower().Trim() == model.username.ToLower().Trim() &&
                        x.Password.ToLower().Trim() == model.pswd.ToLower().Trim() &&
                        x.IsActive == true
                        select x).FirstOrDefault();

            if (user == null)
            {
                result.Errors.Add("Invalid username or password");
                result.StatusCode = APIStatusCode.Unauthorized;
                return result;
            }
            else
            {
                result.Data = new Responce_Auth_Model();
                result.Data.AccessToken = Guid.NewGuid().ToString();
                UserToken ut = new UserToken
                {
                    UserId = user.Id,
                    IsActive = true,
                    IssuedOn = DateTime.Now,
                    ExpiresOn = DateTime.Now.AddMinutes(20),
                    Token = result.Data.AccessToken
                };
                Context.UserTokens.Add(ut);
                Context.SaveChanges();

                result.Data.Id = user.Id;
                result.Data.UserName = user.Username;
                result.Data.IsAdmin = user.UserRoles.Any(x => x.RoleId == 1);
                result.Data.RoleName = string.Join(",", user.UserRoles.Select(x => x.MA_Role.Name));

                result.StatusCode = APIStatusCode.OK;
                return result;
            }
        }


        public DataResult<UserModel> AddEditUser(UserModel model)
        {
            var result = new DataResult<UserModel>();
            if (!model.IsAdmin)
            {
                result.Data = model;
                result.StatusCode = APIStatusCode.OK;
                return result;
            }

            try
            {
                if (model.ProfileImage != null)
                {
                    model.ProfileImage_Name = Regex.Replace(Guid.NewGuid().ToString().Substring(0, 7), @"[^0-9a-zA-Z]+", "") + "_" + Path.GetFileName(model.ProfileImage.FileName);
                    var ServerSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~//Utility//Uploads//ProfileImage//") + model.ProfileImage_Name);
                    model.ProfileImage.SaveAs(ServerSavePath);
                }

                if (model.Id == 0)
                    result.Data = this.AddUser(model);
                else if (model.Id != 0)
                    result.Data = this.EditUser(model);
            }
            catch (Exception ex)
            {
                result.message = ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
                return result;
            }
            result.message = string.IsNullOrEmpty(result.Data.AddBeneficiary.message) ? "SUCCESS" : result.Data.AddBeneficiary.message;
            result.StatusCode = APIStatusCode.OK;
            return result;
        }
        public UserModel AddUser(UserModel model)
        {

            var AddBeneficiary = new DataResult<string>();
            var result = new UserModel();

            var BeneId = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
            if (!string.IsNullOrEmpty(model.Account_Number) && !string.IsNullOrEmpty(model.IFSC))
            {
                var GetToken = Cashfree_Payouts_Config.GetToken();
                AddBeneficiary = Cashfree_Payouts_Config.AddBeneficiary(GetToken.Data, BeneId, model.Name, string.IsNullOrEmpty(model.Email) ? "brksbegs@gmail.com" : model.Email, string.IsNullOrEmpty(model.Mobile) ? "9999999999" : model.Mobile, model.Account_Number, model.IFSC, "Address Not Uploaded");
            }

            var entity = new MA_User
            {
                BeneId = BeneId,
                ProfileImage = model.ProfileImage_Name,
                Name = model.Name,
                Email = string.IsNullOrEmpty(model.Email) ? BeneId + "@gmail.com" : model.Email,
                Username = string.IsNullOrEmpty(model.Username) ? BeneId + "@gmail.com" : model.Username,
                Password = string.IsNullOrEmpty(model.Password) ? BeneId : model.Password,
                IsActive = true,
                Designation = model.Designation,

                Phone = model.Mobile,

                Annual_Basic_Salary = model.Monthly_Basic_Salary == null ? 0 : model.Monthly_Basic_Salary * 12,
                Annual_Special_Allowance = model.Monthly_Special_Allowance == null ? 0 : model.Monthly_Special_Allowance * 12,
                Annual_PF_Amount = model.Monthly_PF_Amount == null ? 0 : model.Monthly_PF_Amount * 12,
                Annual_ESIC = model.Monthly_ESIC == null ? 0 : model.Monthly_ESIC * 12,

                PAN = model.PAN,
                Aadhar = model.Aadhar,
                UAN_No = model.UAN_No,
                PF_No = model.PF_No,
                UserRoles = (from x in model.RoleId select new UserRole { RoleId = Convert.ToInt32(x) }).ToList(),
               

            };
            model.AddBeneficiary = AddBeneficiary;
            Context.MA_User.Add(entity);
            Context.SaveChanges();

            model.Id = entity.Id;
            return model;
        }
        public UserModel EditUser(UserModel model)
        {

            var AddBeneficiary = new DataResult<string>();
            var result = new UserModel();
            var entity = this.Get(model.Id);
            entity.ProfileImage = model.ProfileImage_Name == "noImg.png" ? entity.ProfileImage : model.ProfileImage_Name;
            entity.Name = model.Name;
            entity.Username = model.Username;
            entity.Password = model.Password;
            entity.IsActive = true;
            entity.Designation = model.Designation;
            entity.Email = model.Email;
            entity.Phone = model.Mobile;
            entity.PAN = model.PAN;
            entity.Aadhar = model.Aadhar;
            entity.UAN_No = model.UAN_No;
            entity.PF_No = model.PF_No;

            if (model.IsAdmin)
            {
                entity.Annual_Basic_Salary = model.Monthly_Basic_Salary == null ? 0 : model.Monthly_Basic_Salary * 12;
                entity.Annual_Special_Allowance = model.Monthly_Special_Allowance == null ? 0 : model.Monthly_Special_Allowance * 12;
                entity.Annual_PF_Amount = model.Monthly_PF_Amount == null ? 0 : model.Monthly_PF_Amount * 12;
                entity.Annual_ESIC = model.Monthly_ESIC == null ? 0 : model.Monthly_ESIC * 12;

                Context.UserRoles.RemoveRange(entity.UserRoles.ToList());
                Context.UserRoles.AddRange((from x in model.RoleId select new UserRole { UserId = entity.Id, RoleId = Convert.ToInt32(x) }));
                
            }

            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();

            var GetToken = Cashfree_Payouts_Config.GetToken();
            var RemoveBeneficiary = Cashfree_Payouts_Config.RemoveBeneficiary(GetToken.Data, entity.BeneId);
            Context.SaveChanges();

            if (model.BankId != null && !string.IsNullOrEmpty(model.Account_Number) && !string.IsNullOrEmpty(model.IFSC))
            {
                AddBeneficiary = Cashfree_Payouts_Config.AddBeneficiary(GetToken.Data, entity.BeneId, entity.Name, string.IsNullOrEmpty(entity.Email) ? "brksbegs@gmail.com" : entity.Email, string.IsNullOrEmpty(entity.Phone) ? "9999999999" : entity.Phone, model.Account_Number, model.IFSC, "Address Not Uploaded");


                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();
            }


            model.AddBeneficiary = AddBeneficiary;

            return model;
        }
        public DataResult<UserModel> GetUser(int userid)
        {
            var result = new DataResult<UserModel>();
            try
            {
                var user = this.Get(userid);

                result.Data = new UserModel
                {
                    User = user,
                    ProfileImage_Name = String.IsNullOrEmpty(user.ProfileImage) ? "noImg.png" : user.ProfileImage,
                    Id = user.Id,
                    Name = user.Name,
                    Username = user.Username,
                    Designation = user.Designation,
                    Email = user.Email,
                    Password = user.Password,
                    Mobile = user.Phone,

                    Monthly_Basic_Salary = user.Annual_Basic_Salary <= 0 ? 0 : user.Annual_Basic_Salary / 12,
                    Monthly_Special_Allowance = user.Annual_Special_Allowance <= 0 ? 0 : user.Annual_Special_Allowance / 12,
                    Monthly_PF_Amount = user.Annual_PF_Amount <= 0 ? 0 : user.Annual_PF_Amount / 12,
                    Monthly_ESIC = user.Annual_ESIC <= 0 ? 0 : user.Annual_ESIC / 12,

                    PAN = user.PAN,
                    Aadhar = user.Aadhar,
                    UAN_No = user.UAN_No,
                    PF_No = user.PF_No,
                    RoleId = user.UserRoles.Select(x => x.MA_Role.Id.ToString()).ToList(),
                };

            }
            catch (Exception ex)
            {
                result.message = ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message;
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
                return result;
            }
            result.message = "SUCCESS";
            result.StatusCode = APIStatusCode.OK;
            return result;
        }
        public DataResult<string> DeleteUser(int id)
        {
            var result = new DataResult<string>();
            try
            {
                var entity = this.Get(id);
                //Context.Properties.RemoveRange(entity.Properties);
                //Context.UserRoles.RemoveRange(entity.UserRoles);
                //Context.UserTokens.RemoveRange(entity.UserTokens);
                //Context.UserBankDetails.RemoveRange(entity.UserBankDetails);
                //Context.MA_User.Remove(entity);
                entity.IsActive = false;
                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();

                result.StatusCode = APIStatusCode.OK;
                result.Data = "SUCCESS";
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
                result.StatusCode = APIStatusCode.Oops;
            }
            return result;
        }


        


 


    }


}