﻿using Newtonsoft.Json;
using RealEstate.Database.Entity;
using RealEstate.Models.Project;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RealEstate.Database.Service
{

    public interface IUniversity_Service
    {
        IList<filtersmodel> get_filter();
        IList<MA_UniversityModel> get_university(string searchtext, string intake, string year, string programlevel, string country, string studyarea, string disciplinearea, string duration, string elp, string requirements, string university, string tutionfee);
        DataTable get_university(string universityid);
        MA_University CourseDetail(int uid);
        IList<MA_UniversityModel> compareuniversity(string uid);
    }
    public class University_Service : IUniversity_Service
    {
        private readonly eApp Context;
        public University_Service()
        {
            Context = new eApp();
        }


        public IList<filtersmodel> get_filter()
        {
            var data = Context.Database.SqlQuery<filtersmodel>("usp_filter").ToList();
            return data;
        }

        public IList<MA_UniversityModel> get_university(string searchtext, string intake, string year, string programlevel, string country, string studyarea, string disciplinearea, string duration, string elp, string requirements,string university,string tutionfee)
        {

            var param = new { searchtext, intake, year, programlevel, country, studyarea, disciplinearea, duration, elp, requirements };

            var data = Context.Database.SqlQuery<MA_UniversityModel>("usp_get_university @searchtext, @intake, @year, @programlevel, @country, @studyarea, @disciplinearea, @duration, @elp, @requirements, @university ,@tutionfee",
                new SqlParameter("@searchtext", searchtext),
                new SqlParameter("@intake", intake),
                new SqlParameter("@year", year),
                new SqlParameter("@programlevel", programlevel),
                new SqlParameter("@country", country),
                new SqlParameter("@studyarea", studyarea),
                new SqlParameter("@disciplinearea", disciplinearea),
                new SqlParameter("@duration", duration),
                new SqlParameter("@elp", elp),
                new SqlParameter("@requirements", requirements),
                new SqlParameter("@university", university),
                new SqlParameter("@tutionfee", tutionfee)
                ).ToList();

            var intake_lst = new List<MA_UniversityModel>();
            var country_lst = new List<MA_UniversityModel>();
            var studyarea_lst = new List<MA_UniversityModel>();
            var programlvl_lst = new List<MA_UniversityModel>();
            var requirement_lst = new List<MA_UniversityModel>();

            int flag = 0;
            bool flag_item = false;

            if (data.Count > 0)
            {
                var filter = Context.MA_Filter.ToList();
                //var intake_ = intake.Split(',').ToList();
                //var programlevel_ = programlevel.Split(',').ToList();
                //var country_ = country.Split(',').ToList();
                //var studyarea_ = studyarea.Split(',').ToList();
                var requirements_ = requirements.Split(',').ToList();

                //#region intake_
                //foreach (var u in intake_)
                //{
                //    if (!string.IsNullOrEmpty(u))
                //    {
                //        flag_item = true;
                //        var name = filter.Where(x => x.Id == Convert.ToInt32(u.Trim())).FirstOrDefault().Name;
                //        name = name.ToLower() == "january" ? "Jan" :
                //              name.ToLower() == "february" ? "Feb" :
                //              name.ToLower() == "march" ? "Mar" :
                //              name.ToLower() == "april" ? "Apr" :
                //              name.ToLower() == "may" ? "May" :
                //              name.ToLower() == "june" ? "Jun" :
                //              name.ToLower() == "july" ? "Jul" :
                //              name.ToLower() == "august" ? "Aug" :
                //              name.ToLower() == "september" ? "Sep" :
                //              name.ToLower() == "october" ? "Oct" :
                //              name.ToLower() == "november" ? "Nov" :
                //              name.ToLower() == "deccember" ? "Dec" : name;

                //        var lst = data.Where(x => x.Intakes.ToLower().Contains(name.ToLower())).ToList();
                //        foreach (var v in lst)
                //        {
                //            intake_lst.Add(v);
                //        }

                //    }
                //}
                //if (flag_item == true)
                //{
                //    flag = 1;
                //    flag_item = false;
                //}

                //#endregion

                //#region country_
                //foreach (var u in country_)
                //{
                //    //intake 1
                //    //data 0
                //    if (!string.IsNullOrEmpty(u))
                //    {
                //        flag_item = true;
                //        var name = filter.Where(x => x.Id == Convert.ToInt32(u.Trim())).FirstOrDefault().Name;
                //        var lst = flag == 1 ? intake_lst.Where(x => x.Country.ToLower().Contains(name.ToLower())).ToList() :
                //            data.Where(x => x.Country.ToLower().Contains(name.ToLower())).ToList();
                //        foreach (var v in lst)
                //        {
                //            country_lst.Add(v);
                //        }
                //    }
                //}

                //if (flag_item == true)
                //{
                //    flag = 2;
                //    flag_item = false;
                //}

                //#endregion

                //#region studyarea_
                //foreach (var u in studyarea_)
                //{
                //    //country 2
                //    //intake 1
                //    //data 0
                //    if (!string.IsNullOrEmpty(u))
                //    {
                //        flag_item = true;
                //        var lst =
                //            flag == 2 ? country_lst.Where(x => x.CategoryId.ToLower().Contains(u.ToLower())).ToList() :
                //            flag == 1 ? intake_lst.Where(x => x.CategoryId.ToLower().Contains(u.ToLower())).ToList() :
                //            data.Where(x => x.CategoryId.ToLower().Contains(u.ToLower())).ToList();
                //        foreach (var v in lst)
                //        {
                //            studyarea_lst.Add(v);
                //        }
                //    }
                //}
                //if (flag_item == true)
                //{
                //    flag = 3;
                //    flag_item = false;
                //}

                //#endregion

                //#region programlevel_
                //foreach (var u in programlevel_)
                //{
                //    //study area 3
                //    //country 2
                //    //intake 1
                //    //data 0
                //    if (!string.IsNullOrEmpty(u))
                //    {
                //        flag_item = true;
                //        var name = filter.Where(x => x.Id == Convert.ToInt32(u.Trim())).FirstOrDefault().Name;
                //        var lst = flag == 3 ? studyarea_lst.Where(x => x.Studylvl.ToLower().Contains(name.ToLower())).ToList() :
                //           flag == 2 ? country_lst.Where(x => x.Studylvl.ToLower().Contains(name.ToLower())).ToList() :
                //            flag == 1 ? intake_lst.Where(x => x.Studylvl.ToLower().Contains(name.ToLower())).ToList() :
                //            data.Where(x => x.Studylvl.ToLower().Contains(name.ToLower())).ToList();
                //        foreach (var v in lst)
                //        {
                //            programlvl_lst.Add(v);
                //        }
                //    }
                //}
                //if (flag_item == true)
                //{
                //    flag = 4;
                //    flag_item = false;
                //}
                //#endregion

                #region requirements_
                foreach (var u in requirements_)
                {
                    //program level 4
                    //study area 3
                    //country 2
                    //intake 1
                    //data 0
                    if (!string.IsNullOrEmpty(u))
                    {
                        flag_item = true;
                        var name = filter.Where(x => x.Id == Convert.ToInt32(u.Trim())).FirstOrDefault().Name;

                        var lst = flag == 4 ? programlvl_lst.ToList() :
                            flag == 3 ? studyarea_lst.ToList() :
                            flag == 2 ? country_lst.ToList() :
                            flag == 1 ? intake_lst.ToList() :
                            data.ToList();

                        var filtered_lst = new List<MA_UniversityModel>();
                        if (name.ToLower() == "SAT".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.SatRequired) && Convert.ToBoolean(x.SatRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "GMAT".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.GmatRequired) && Convert.ToBoolean(x.GmatRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "Without GMAT".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.GmatRequired) && Convert.ToBoolean(x.GmatRequired.Trim()) == false).ToList();
                        else if (name.ToLower() == "ACT".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.ActRequired) && Convert.ToBoolean(x.ActRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "GRE".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.GreRequired) && Convert.ToBoolean(x.GreRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "Without GRE".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.GreRequired) && Convert.ToBoolean(x.GreRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "IELTS".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.IeltsRequired) && Convert.ToBoolean(x.IeltsRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "TOEFL iBT".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.ToeflRequired) && Convert.ToBoolean(x.ToeflRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "PTE".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.PteRequired) && Convert.ToBoolean(x.PteRequired.Trim()) == true).ToList();
                        else if (name.ToLower() == "Scholarship Available".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.ScholarshipAvailable) && Convert.ToBoolean(x.ScholarshipAvailable.Trim()) == true).ToList();
                        else if (name.ToLower() == "Online Programmes".ToLower())
                            filtered_lst = lst.Where(x => !string.IsNullOrEmpty(x.IsOnlineCourse) && Convert.ToBoolean(x.IsOnlineCourse.Trim()) == true).ToList();
                        else
                            filtered_lst = lst;



                        foreach (var v in filtered_lst)
                        {
                            requirement_lst.Add(v);
                        }
                    }
                }
                if (flag_item == true)
                {
                    flag = 5;
                    flag_item = false;
                }
                #endregion

            }

            var datacount = flag == 0 ? data.Distinct().ToList().Count :
                flag == 1 ? intake_lst.Distinct().ToList().Count :
                flag == 2 ? country_lst.Distinct().ToList().Count :
                flag == 3 ? studyarea_lst.Distinct().ToList().Count :
                flag == 4 ? programlvl_lst.Distinct().ToList().Count :
                 requirement_lst.Distinct().ToList().Count;

            //data.ForEach(x=>x.Count=)
            var resp = flag == 0 ? data.Distinct().ToList() :
                flag == 1 ? intake_lst.Distinct().ToList() :
                flag == 2 ? country_lst.Distinct().ToList() :
                flag == 3 ? studyarea_lst.Distinct().ToList() :
                flag == 4 ? programlvl_lst.Distinct().ToList() :
                 requirement_lst.Distinct().ToList();


            resp.ForEach(x => x.Count = datacount.ToString());
            return resp;
        }

        public DataTable get_university(string universityid)
        {

            String constring = Context.Database.Connection.ConnectionString;
            SqlConnection con = new SqlConnection(constring);
            string query = "exec usp_export_university '" + universityid + "'";
            DataTable dt = new DataTable();
            dt.TableName = "University";
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(query, con);
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public MA_University CourseDetail(int uid)
        {
            return Context.MA_University.Find(uid);
        }

        public IList<MA_UniversityModel> compareuniversity(string uid)
        {
            var data = Context.Database.SqlQuery<MA_UniversityModel>("usp_compare_university @uid",
               new SqlParameter("@uid", uid)
               ).ToList();

            return data;

        }
    }
}