﻿using ClosedXML.Excel;
using Microsoft.Ajax.Utilities;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models.Project;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RealEstate.Controllers.Project
{
    public class universityController : Controller
    {
        private readonly IUniversity_Service University_Service;
        public universityController()
        {
            University_Service = new University_Service();
        }
        public ActionResult Index()
        {
            // var data = University_Service.get_filter();
            var model = new List<MA_UniversityModel>();
            return View(model);
        }

        public ActionResult SearchProgram()
        {
            // var data = University_Service.get_filter();
            return View("Index");
        }



        [HttpGet]
        public JsonResult get_filter()
        {
            var data = University_Service.get_filter();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult get_university(string searchtext, string intake, string year, string programlevel, string country, string studyarea, string disciplinearea, string duration, string elp, string requirements, string university, string tutionfee, bool ispagefilter = false)
        {
            var data = University_Service.get_university(searchtext, intake, year, programlevel, country, studyarea, disciplinearea, duration, elp, requirements, university, tutionfee);
            //var university = (from x in data select x.University).GroupBy(x => x).ToList();
            //var university = (from x in data select x.University).Distinct().ToList();
            //ViewBag.university = university;
            //return PartialView("_universitylist", data);

            if (!ispagefilter)
                return PartialView("_Result", data);
            else
                return PartialView("_universitylist", data);
        }

        [HttpGet]
        public string export(string Universityid)
        {
            var dir = Server.MapPath("~/Utility/Uploads/export/");
            System.IO.DirectoryInfo di = new DirectoryInfo(dir);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            var filename = "Universitylist" + Guid.NewGuid().ToString() + ".xlsx";


            var dt = University_Service.get_university(Universityid);
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                //Response.Clear();
                //Response.Buffer = true;
                //Response.Charset = "";
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", "attachment;filename= Universitylist.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(dir + filename);
                    // wb.SaveAs(MyMemoryStream);
                    //MyMemoryStream.WriteTo(Response.OutputStream);
                    //Response.Flush();
                    //Response.End();
                }
            }
            var obj = new
            {
                filename = filename
            };
            string json = JsonConvert.SerializeObject(obj);
            return json;
        }


        public ActionResult CourseDetail(int uid)
        {
            var data = University_Service.CourseDetail(uid);
            return View(data);
        }

        [HttpGet]
        public JsonResult compareuniversity(string uid)
        {
            var data = University_Service.compareuniversity(uid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }

}