﻿using RealEstate.Config;
using RealEstate.Database.Entity;
using RealEstate.Database.Service;
using RealEstate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RealEstate.Controllers.API
{
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        private readonly IMA_User_Service MA_User_Service;

        public DashboardController()
        {
            MA_User_Service = new MA_User_Service();
        }
        

        [HttpPost, Route("signup")]
        public DataResult<string> signup(SignUpModel model)
        {
            return MA_User_Service.SignUp(model);
        }
        [HttpPost, Route("login")]
        public DataResult<Responce_Auth_Model> login(Req_Auth_Model model)
        {
            return MA_User_Service.AccessToken(model);
        }

        [HttpPost, Route("forgetpassword")]
        public DataResult<string> forgetpassword(ForgetPasswordModel model)
        {
            return MA_User_Service.ForgetPassword(model);
        }

         

    }
}
