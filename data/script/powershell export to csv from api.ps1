﻿$session = New-Object Microsoft.PowerShell.Commands.WebRequestSession
$session.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36"
$session.Cookies.Add((New-Object System.Net.Cookie("_hjid", "4cad52cd-2efc-42c6-b33a-8a53c0366795", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_ga", "GA1.2.1806835389.1635438390", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_hjSessionUser_2238237", "eyJpZCI6IjY1MTMwZjNjLWIyNzMtNWQ2NS1iYjA5LWM3NmZjNzQxYTJlYiIsImNyZWF0ZWQiOjE2Mzc0Njc1OTI2NzEsImV4aXN0aW5nIjp0cnVlfQ==", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("ARRAffinity", "7c89060fd0d0c44cf5ebe61bec8e6153801788be235221c9d628ec913119c053", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("ARRAffinitySameSite", "7c89060fd0d0c44cf5ebe61bec8e6153801788be235221c9d628ec913119c053", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("__RequestVerificationToken", "1uCREi0wwn3yaCBfn2T0FLHHIk8iAov9fG8-H-FK1QvCRXS9H2A2fbZ7ebVAv7_wn_1dYRPej4R16ghxkBosWFIb8nPfJL4cDgoA8eNYmSc1", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_gid", "GA1.2.40811339.1637683996", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("OpenIdConnect.nonce.gAhGZWmIT%2BPD%2FpB44UKu7uMLgLvmrhgLeRay%2FRlxGys%3D", "YUoyYmJ5bXRYaHZPV0QtdUM2aXJDVDBUT3JzeWlJZVRnUmFqV1JLUUJLYjJ2UmF2Uko1dHhYaXJ4LThpekxwR0ZXb0dkWU9fR2t1NjI3dXZMZmhoTnhCcTgzWkNzMWR1UGtCOWxwMHM3dUpkcHZ5NFlTWFJnZFpHbWx5dHF3LWo1aE9BbFgzck1hRko4VmVHLTk2TVBLcEtsZDJLcU95UFF2TmFfNGtBQjk4VGtCZkcwaUFTQW1sS1MtN1hHUVBDR2gxRW9YUlBCWE1pNmxBNDdXVV9rLXdDNk5wR0VDX2NaUE84MDUtTnRiMA%3D%3D", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("OpenIdConnect.nonce.Gvjp1ju50m0z6ioXyMytG2PwefeVQUY2mcYjJ3lYvaI%3D", "NXJ5RXVmOXdLdHhXTWVlRjBEbFF5b3NqZU1UOTNTaUZjM2dTaDV0dmdHY1JlblRsLWpHZ1B3Qlc2THJld1BYVUFmbUFQcnIxQzV6em9IblA2Xy16aEVvVFlNNXgtM3dMUC16ZWJjWFhtdjljUTJCNDNiMnFKelVnbS1nTTRjSXBrZjN0aFdnQWRnV3cwTWtxWFAydzllQ044QzhuMV9NdWR3eVBZdXh4SG9kRWMwOWVZNHRGZmhYVjlvWEt5c3JoZV9KeEpvU0tBdlhab1M1Z09kNmdLVHBsWk5IclFrTHM3ODRveEhjM0R4WQ%3D%3D", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie(".AspNet.Cookies", "_It_d03_MY0klQdJr09V5Zw4kqFjZKcKUwVseoHSL-_7BsYEeZb2l9099V4DzJ4fHz0yD14TM_AHtcBtLQSnOKSDhxgXvtbBj3e-FGMbWU7GvQNr2qSawW7MAH1obqfln4ZKOxByFHynOapNneM6zzYAIBVSwlvxwW-a5BnKyddONwq2A0DIPGo5_lHMK7lDEIV-XbwZ6oJjkuiKNZ12IvWCLCMiTzJ4nkDrTX9cIYSN3oP3-COm8uAyZO-SyrR9KfeJb9Ax-Lj48kjEesX3yd71F8nqvkhMshXi2jdZRvif8AO2iAjuqPXdBRGuB2pwTeZzB7uTKgkjH4ha0iPfqu03QNntPWjIAI7sV0wlIrTlqSEuW5a_9qVUei5lALZBixMMe008XJC0QPf0qR3AHmvwDDp0dm0_WiG663Zp9YFEewwkdtwbv2_Rh5eeh4pYWGkpBmkvlNLGFShNFvWg3YuJwcuWa_GInClkNFqVdZUwdvWUkMAh1WL-bghmijHZfB77LqofWDaOcUd1NeAheOjhexDW3jBxevCZqMoQbzSj2N2rP1B1rmy7_DB_qV1WVS1yS_kq4WWwMcQhDBHPF8U_D5aX3qFuzxReht06qQ-xAAjAJSoXlsm4RYg-_EnTnUBxz1o9-1NWTvYlupH_aL4ll0_WsoPyibpcaJQUXK7h-aJvmpyjTeVat6up9s-x2SxFtE25W-RYNUNXQN0biz2avAZOtzvEdgHwCsb0gzoOHa9tbGW0Hv54GGnl70uAXxtt3fyLi-9kY4sE1WjbQqu5v794iSGuamnwnZzsoN9iLL3PceF91MwHUR4ATDPtIlRjlROF94RpoZBmVHAd3aA_LGF92nfXrAIJpJYyOR5pu-Gx0DBi24hBlI8tiT06MeQOXZAfjN3IGVOuw3qHE_vEvgcQtg_uu5Cvv3ZLJDAm8m2QLhqEXlc4dwjhMdqKo97TM5q1GACf7uESLi-2eCUiuS2b62Jfg6DEZXFVJJGRwA_90pw2ipFyoAuKkB0ZYPCpxcbv7u7doz8aLtuEEyuMlyBq_XrmTVqKRx234HSnqiv1sro9_lqKpzEowRQNl7oaBkH5JjigwT698XeQ7Q", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("ASP.NET_SessionId", "spotqew51sleq5xxfuu1gwil", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_hjSession_2238237", "eyJpZCI6ImQyNjM3YzA4LWZiM2EtNDk4Zi05N2ZiLWJiNTQwNGUyYjg0ZSIsImNyZWF0ZWQiOjE2Mzc3MjE4OTA5MTJ9", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_hjAbsoluteSessionInProgress", "0", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_gat_gtag_UA_131900023_1", "1", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("unSeenNotifCount", "", "/", "coursefinder.ai")))
$res=Invoke-WebRequest -UseBasicParsing -Uri "https://coursefinder.ai/SearchProgram/_Search?Length=13" `
-Method "POST" `
-WebSession $session `
-Headers @{
"sec-ch-ua"="`" Not A;Brand`";v=`"99`", `"Chromium`";v=`"96`", `"Google Chrome`";v=`"96`""
  "X-NewRelic-ID"="VwQOU19aABABVVBbBwAAVFcD"
  "tracestate"="3292998@nr=0-1-3292998-1119017708-1861d648822ea02d----1637721900861"
  "traceparent"="00-ca0370a7d4ebf57874e6d570e09263f0-1861d648822ea02d-01"
  "sec-ch-ua-mobile"="?0"
  "newrelic"="eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjMyOTI5OTgiLCJhcCI6IjExMTkwMTc3MDgiLCJpZCI6IjE4NjFkNjQ4ODIyZWEwMmQiLCJ0ciI6ImNhMDM3MGE3ZDRlYmY1Nzg3NGU2ZDU3MGUwOTI2M2YwIiwidGkiOjE2Mzc3MjE5MDA4NjF9fQ=="
  "Accept"="*/*"
  "X-Requested-With"="XMLHttpRequest"
  "sec-ch-ua-platform"="`"Windows`""
  "Origin"="https://coursefinder.ai"
  "Sec-Fetch-Site"="same-origin"
  "Sec-Fetch-Mode"="cors"
  "Sec-Fetch-Dest"="empty"
  "Referer"="https://coursefinder.ai/SearchProgram"
  "Accept-Encoding"="gzip, deflate, br"
  "Accept-Language"="en-US,en;q=0.9"
} `
-ContentType "application/x-www-form-urlencoded; charset=UTF-8" `
-Body "subCategorySelection=-1&txtsearch=&IntakeList=0&IntakeList=1&IntakeList=2&IntakeList=3&IntakeList=4&IntakeList=5&IntakeList=6&IntakeList=7&IntakeList=8&IntakeList=9&IntakeList=10&IntakeList=11&IntakeList=12&IntakeList=13&IntakeList=14&IntakeList=15&IntakeList=16&hdnBoardOfExamination=&hdnMarks=&hdnSortBy=&Year=2022&DurationList=&ESLELPAvailable=&X-Requested-With=XMLHttpRequest"

$res
$res.Content | ConvertFrom-Json | Select-Object -Property *
$js = ConvertFrom-Json $([String]::new($res.Content))
$js | Export-csv "C:\Users\VishalKumar\Documents\vishal\Projects\aussieseducation\data\University.csv"  -NoTypeInformation