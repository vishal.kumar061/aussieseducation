Get-Process *Note* | Select-object | Export-csv xxx.csv -NoTypeInformation

'[{"A":123}, {"B":456}]' | ConvertFrom-Json | Format-List *
$res.Content | ConvertFrom-Json | Format-List *

'[{"A":123}, {"B":456}]' | ConvertFrom-Json | Select-Object -Property *
$res.Content | ConvertFrom-Json | Select-Object -Property *


$data | Export-Csv -Path "C:\Users\VishalKumar\Documents\vishal\Projects\aussieseducation\result1.csv" -NoTypeInformation

$csv = ConvertTo-Csv $([String]::new($res.Content)) 

$js = ConvertFrom-Json $([String]::new($res.Content)) 
$js | Export-csv -Encoding UTF8 "C:\Users\VishalKumar\Documents\vishal\Projects\aussieseducation\result1.txt" 

$res.Content| ConvertTo-Json -depth 100 | Out-File "C:\Users\VishalKumar\Documents\vishal\Projects\aussieseducation\result1.txt" -Encoding UTF8
$res.Content| ConvertTo-Csv | Out-File "C:\Users\VishalKumar\Documents\vishal\Projects\aussieseducation\result1.csv" -Encoding UTF8


$session = New-Object Microsoft.PowerShell.Commands.WebRequestSession
$session.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36"
$session.Cookies.Add((New-Object System.Net.Cookie("_hjid", "4cad52cd-2efc-42c6-b33a-8a53c0366795", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_ga", "GA1.2.1806835389.1635438390", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_hjSessionUser_2238237", "eyJpZCI6IjY1MTMwZjNjLWIyNzMtNWQ2NS1iYjA5LWM3NmZjNzQxYTJlYiIsImNyZWF0ZWQiOjE2Mzc0Njc1OTI2NzEsImV4aXN0aW5nIjp0cnVlfQ==", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("ARRAffinity", "7c89060fd0d0c44cf5ebe61bec8e6153801788be235221c9d628ec913119c053", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("ARRAffinitySameSite", "7c89060fd0d0c44cf5ebe61bec8e6153801788be235221c9d628ec913119c053", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie(".AspNet.Cookies", "py0C850mbu4Kq2jxSzt8rDT-_z5hvxHi7yaRuWpJrmpMrNC3K3gibMp-b6w0zZTMlk9xpe-jWayFKtNYJ6ExrBLLRmB3JTyIU-mB3Qm9YMK9xb1c0UQZCvR2MkihwUtRxgrXs4mf_xNSUAxzIP7sDiaU_xRQ-sfiDt9bP_Nr8wopfYvSSQhu6_JTula8S7K6WOCMhYt3yZ9lqSWIkICzCsOZXbDpbJqH05iR0AheRZmdCf3PfpvqaA2ErIGkUTftTnJARnfJWyegF5FZVxsr7EwOTn_rAoxktoyon91X6OByekOGri4OIjDV3NPVwlWecSADCMB-5GreiSEHlTkkgcYTB9XQOko29842UZ__5DeXtEtYuONCJZ7KmKWTVH-eVKdpo_BVKey9Wxm9GEuwh9E3VXftqzcbvqOhFIiSxoOyBl3wMyBDoK-Oy1hNuNereTAMcIFQYf-Yv7C_l_gx94Rk43g_-JwN0tF2wcOLXf44ctgu0tHokDMbLMfkzEZ5fTzAqWKLj5Aa2Rx0UAvkkKbuMLgXCUjYP9K20YZEP_RHckm_PX-i6Wgry6IyCLCGxSNBtnHbsbbywsz_g0ER2gdKBcqrWxCGK41pOpouWhBAr-qRiyPhx7qCgJL8eCXKOE9knDIQhxffTxF5oIp8cvy_ebooBn_bg1ZjKkF9gznU4-Y2lxRLpKoF7wlFMVrf0918JSrKCSmCtgkFQyRvYp0p_L88tdqy39SlUb_Koi2UC2WnZ3umuAXDvmJ53P-nmM-y9EWWSEQUDLUyOZKca0wDki0r2BhStVrI66dHaQedf7lCB9LygNbAwzRSeRPt-mIQ99iUaSVZXSj0L8-GI3Rqx39nOnp-cSZbKH16lhM7K87ItVgV0CDGxxGyjQb73kNGbrVIKIix9bE_HNQg02D8Uf4g227cY-WMzqqPD-sqGyHtKo3Ev_5AJxB_8cV-TvOK9dqWrNqJGag-2khXi59ONrT8vCMpxJWEKKG0paqMB5KZLxc1p7zRwSG2Np2_U005JaNPnI8M8ryk3rkI8usAH4mStm1nv-At2P7zDWIRJAhj0rKmYn0fKp6MdIbsejVL4pFakpYBKT0FIG4R8A", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("ASP.NET_SessionId", "nkin1djfxxichwka5om134ih", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("__RequestVerificationToken", "1uCREi0wwn3yaCBfn2T0FLHHIk8iAov9fG8-H-FK1QvCRXS9H2A2fbZ7ebVAv7_wn_1dYRPej4R16ghxkBosWFIb8nPfJL4cDgoA8eNYmSc1", "/", "coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_gid", "GA1.2.40811339.1637683996", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_hjSession_2238237", "eyJpZCI6IjA0YjVhMWFiLTQzNGUtNGVlNy04YmZmLWRiNzk2MTYzYmE5NCIsImNyZWF0ZWQiOjE2Mzc2ODM5OTU2ODZ9", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("_hjAbsoluteSessionInProgress", "0", "/", ".coursefinder.ai")))
$session.Cookies.Add((New-Object System.Net.Cookie("unSeenNotifCount", "", "/", "coursefinder.ai")))
$res=Invoke-WebRequest -UseBasicParsing -Uri "https://coursefinder.ai/SearchProgram/_Search?Length=13" `
-Method "POST" `
-WebSession $session `
-Headers @{
"sec-ch-ua"="`" Not A;Brand`";v=`"99`", `"Chromium`";v=`"96`", `"Google Chrome`";v=`"96`""
  "X-NewRelic-ID"="VwQOU19aABABVVBbBwAAVFcD"
  "tracestate"="3292998@nr=0-1-3292998-1119017708-ccf523603e850e12----1637684187754"
  "traceparent"="00-6fed064a145f31881a5e9a929a69c8e0-ccf523603e850e12-01"
  "sec-ch-ua-mobile"="?0"
  "newrelic"="eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjMyOTI5OTgiLCJhcCI6IjExMTkwMTc3MDgiLCJpZCI6ImNjZjUyMzYwM2U4NTBlMTIiLCJ0ciI6IjZmZWQwNjRhMTQ1ZjMxODgxYTVlOWE5MjlhNjljOGUwIiwidGkiOjE2Mzc2ODQxODc3NTR9fQ=="
  "Accept"="*/*"
  "X-Requested-With"="XMLHttpRequest"
  "sec-ch-ua-platform"="`"Windows`""
  "Origin"="https://coursefinder.ai"
  "Sec-Fetch-Site"="same-origin"
  "Sec-Fetch-Mode"="cors"
  "Sec-Fetch-Dest"="empty"
  "Referer"="https://coursefinder.ai/SearchProgram"
  "Accept-Encoding"="gzip, deflate, br"
  "Accept-Language"="en-US,en;q=0.9"
} `
-ContentType "application/x-www-form-urlencoded; charset=UTF-8" `
-Body "subCategorySelection=-1&txtsearch=australia&IntakeList=0&IntakeList=1&IntakeList=2&IntakeList=3&IntakeList=4&IntakeList=5&IntakeList=6&IntakeList=7&IntakeList=8&IntakeList=9&IntakeList=10&IntakeList=11&IntakeList=12&IntakeList=13&IntakeList=14&IntakeList=15&IntakeList=16&hdnBoardOfExamination=&hdnMarks=&hdnSortBy=&Year=2021&DurationList=&ESLELPAvailable=&X-Requested-With=XMLHttpRequest"



